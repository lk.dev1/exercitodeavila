from app import db

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(80))
    email = db.Column(db.String(120), unique=True)
    senha = db.Column(db.String(80))
    idade = db.Column(db.Integer)
